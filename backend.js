﻿var url = require('url');
var count = 0;
var isAdmin = false;

module.exports = function (request, response) {
    var parsedUrl = url.parse(request.url, true);
    var params = parsedUrl.query;
    var isValidCredentials = params.login === 'admin' && params.password === 'secret';
    var isLoginUrl = parsedUrl.href === '/login.html';

    if (!isAdmin && !isLoginUrl && !isValidCredentials) {
        response.setHeader('Content-type', `text/html; charset=utf-8`);
        response.end(
            `Упс... Для доступа на сайт необходимо авторизоваться<br />
			Через 5 секунд Вы будете перенаправлены на страницу авторизации /login.html<br />
			Передайте параметры login: admin, password: secret
			<script>
				setTimeout(function() {window.location.href="/login.html"}, 5000)
			</script>`
        );

        return false;
    } else if (isValidCredentials) {
        isAdmin = true;
    }

    count = count + 1;
    console.log('Бекенд обработал запрос #', {isAdmin, isLoginUrl, isValidCredentials});
}

