﻿var encoding = 'utf-8';
var http = require('http');
var url = require('url');
var fs = require('fs');
var path = require('path');

var backend = require('./backend.js');

// Можно изменить порт при запуске, например: node server.js 3000
var port = process.argv[2] || 80;

// Переводит расширение файла в MIME тип
var mimeType = {
    '.ico': 'image/x-icon',
    '.html': 'text/html',
    '.js': 'text/javascript',
    '.json': 'application/json',
    '.css': 'text/css',
    '.png': 'image/png',
    '.jpg': 'image/jpeg',
    '.jpeg': 'image/jpeg',
    '.wav': 'audio/wav',
    '.mp3': 'audio/mpeg',
    '.svg': 'image/svg+xml',
    '.pdf': 'application/pdf',
    '.doc': 'application/msword',
    '.eot': 'appliaction/vnd.ms-fontobject',
    '.ttf': 'aplication/font-sfnt'
};

http.createServer(function (req, res) {
    console.log(`${req.method} ${req.url}`);

    // Парсит URL
    var parsedUrl = url.parse(req.url);

    var sanitizePath = path.normalize(parsedUrl.pathname).replace(/^(\.\.[\/\\])+/, '');

    if (false === backend(req, res)) {
        return;
    }

    var pathname = path.join(__dirname, 'public', sanitizePath);
    fs.exists(pathname, function (exist) {
        if (!exist) {
            // Если файл не найден возвращает 404
            res.statusCode = 404;
            res.setHeader('Content-type', `text/plain; charset=${encoding}`);
            res.end(`Файл ${pathname} не найден!`);
            return;
        }

        // Если директория, то добавляет index.html
        if (fs.statSync(pathname).isDirectory()) {
            pathname += '/index.html';
        }

        // Читает файл из папки public
        fs.readFile(pathname, function (err, data) {
            if (err) {
                res.statusCode = 500;
                res.setHeader('Content-type', `text/plain; charset=${encoding}`);
                res.end(`Ошибка при чтении файла: ${err}.`);
            } else {
                // Файл прочитан
                // Получает расширение файла
                var ext = path.parse(pathname).ext;

                // устанавливает Content-type из переменной mimeType
                if (mimeType[ext]) {
                    res.setHeader('Content-type', `${mimeType[ext]}; charset=${encoding}`);
                } else {
                    res.setHeader('Content-type', `text/plain; charset=${encoding}`);
                }

                // Отправляет данные обратно клиенту
                res.end(data);
            }
        });
    });
}).listen(parseInt(port));

console.log(`Сервер запущен на порту ${port}`);
